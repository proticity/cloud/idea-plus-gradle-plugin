/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2019 John Stewart.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.proticity.gradle.plugin.idea

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.plugins.ide.idea.model.IdeaModel
import org.gradle.plugins.ide.idea.model.IdeaModule
import org.gradle.plugins.ide.idea.model.IdeaProject
import org.gradle.plugins.ide.idea.model.IdeaWorkspace
import org.gradle.util.ConfigureUtil

/**
 * An extension class for configuring the plugin.
 */
@CompileStatic
class IdeaPlusExtension {
    IdeaModule module

    IdeaProject project

    IdeaWorkspace workspace

    IdeaPlusConfig config;

    String targetVersion

    IdeaPlusExtension(final IdeaModel idea) {
        module = idea.module
        this.project = idea.project
        workspace = idea.workspace
        targetVersion = idea.targetVersion
        config = new IdeaPlusConfig()
    }

    void module(Action<? super IdeaModule> action) {
        action.execute(module)
    }

    void module(Closure closure) {
        ConfigureUtil.configure(closure, module)
    }

    void project(Closure closure) {
        ConfigureUtil.configure(closure, project)
    }

    void project(Action<? super IdeaProject> action) {
        action.execute(project)
    }

    void workspace(Closure closure) {
        ConfigureUtil.configure(closure, workspace)
    }

    void workspace(Action<? super IdeaWorkspace> action) {
        action.execute(workspace)
    }
    void config(Closure closure) {
        ConfigureUtil.configure(closure, config)
    }

    void config(Action<? super IdeaPlusConfig> action) {
        action.execute(config)
    }
}
