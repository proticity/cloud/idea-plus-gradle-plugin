/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2019 John Stewart.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.proticity.gradle.plugin.idea

import groovy.transform.CompileStatic
import org.gradle.api.internal.ConventionTask
import org.gradle.api.resources.TextResource
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction

import java.nio.file.Paths

@CompileStatic
class ApplyIdeaCodeStyle extends ConventionTask {
    @Input
    boolean useProjectStyle

    @Input
    TextResource codeStyle

    @TaskAction
    void applyCodeStyle() {
        if (!getCodeStyle()) {
            project.logger.info('No IDEA code style provided, skipping code style application.')
            return
        }

        for (def dir = project.projectDir; dir; dir = dir.parentFile) {
            def ideaDir = new File(Paths.get(dir.path, '.idea').toString())
            if (ideaDir.exists() && ideaDir.isDirectory()) {
                project.logger.info("Found IDEA project directory at ${ideaDir.path}")
                def codeStylesDir = new File(Paths.get(ideaDir.path,'codeStyles').toString())
                if (!codeStylesDir.exists()) {
                    if (!codeStylesDir.mkdir()) {
                        // TODO: Error
                    }
                } else if (!codeStylesDir.isDirectory()) {
                    // TODO: Error
                }

                def projectCodeStyle = new File(Paths.get(codeStylesDir.path, 'Project.xml').toString())
                def codeStyleConfig = new File(Paths.get(codeStylesDir.path, 'codeStyleConfig.xml').toString())

                projectCodeStyle.write(getCodeStyle().asString())
                if (isUseProjectStyle()) {
                    def config = getClass().getClassLoader().getResourceAsStream('idea/codeStyleConfig.xml')
                    codeStyleConfig.write(config.text)
                }

                break
            }
        }
    }
}
