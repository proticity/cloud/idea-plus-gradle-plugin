/*
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2019 John Stewart.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.proticity.gradle.plugin.idea

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.plugins.ide.idea.IdeaPlugin
import org.gradle.plugins.ide.idea.model.IdeaModel

/**
 * The plugin which adds the IDEA-Plus functionality.
 */
class IdeaPlusPlugin implements Plugin<Project> {
    @Override
    void apply(final Project project) {
        project.plugins.apply(IdeaPlugin)

        def ideaExt = project.extensions.findByType(IdeaModel)
        def ext = project.extensions.create('ideaPlus', IdeaPlusExtension, ideaExt)
        project.afterEvaluate {
            project.extensions.findByType(IdeaModel).with {
                it.project = ext.project
                module = ext.module
                workspace = ext.workspace
                targetVersion = ext.targetVersion
            }
        }

        project.tasks.create('applyIdeaCodeStyle', ApplyIdeaCodeStyle)
        project.tasks.withType(ApplyIdeaCodeStyle) { ApplyIdeaCodeStyle t ->
            t.conventionMapping.with {
                codeStyle = { ext.config.codeStyle }
                useProjectStyle = { ext.config.useProjectStyle }
            }
        }
    }
}
