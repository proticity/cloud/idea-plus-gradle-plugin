# IDEA-Plus Gradle Plugin
The IDEA-Plus Gradle plugin is an enhancement to the built-in IDEA plugin included with Gradle which improves the IDE
integration, available in the [Gradle Plugin Portal](https://plugins.gradle.org/plugin/org.proticity.gradle.idea-plus).

## Usage
Include the plugin either by using the new plugin dependency spec:
```groovy
plugins {
    id 'org.proticity.gradle.idea-plus' version '0.1.1'
}
```
... or by using the old style of plugin declarations:
```groovy
buildscript {
    repositories {
        maven {
            url 'https://plugins.gradle.org/m2/'
        }
        jcenter()
    }
    dependencies {
        classpath 'org.proticity.gradle:idea-plus-gradle-plugin:0.1.1'
    }
}
plugin apply: 'org.proticity.gradle.idea-plus'
```

The `ideaPlus` extension replaces the standard `idea` extension and supports the same properties, and the same IDEA
tasks are still present. In addition it adds some new functionality. Any configuration that normally would go into the
`idea` extension should now be placed in `ideaPlus` instead, as well as the new configuration.

## Code Styles
The IDEA-Plus plugin can apply a code style to an IDEA project. The task `applyIdeaCodeStyle` will start in the Gradle
project directory and look for an IntelliJ project (a `.idea` directory or `.ipr` file). If one is not found it will
recursively search the parent directory until it finds the project. The Gradle project must be at the same directory or
a subdirectory of the IntelliJ project to find the project files.

When a project is found the plugin will set the code style as the project code style. When IntelliJ is configured to
use the per-project code style (as opposed to the global default) the code style will be used. Optionally Gradle can
force the project to use the per-project code style.

```groovy
ideaPlus {
    config {
        codeStyle = resources.text.fromFile('src/main/codestyles/idea-codestyle.xml')
        useProjectStyle = true
    }
}
```
